export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  publicRuntimeConfig: {
    recaptcha: {
      hideBadge: true,
      siteKey:"6LcM2tMZAAAAAOg9KcJeKqSNz2sOe2rfbm2vZ6TD",
      version: 3,
      size: "invisible"
    },
    axios: {
      browserBaseURL: 'http://localhost:8000/api'
    }
  },
  privateRuntimeConfig: {
    axios: {
      baseURL: 'http://localhost:8000/api'
    }
  },
  head: {
    title: 'Element Seven - Affordable Web Design Belfast, Northern Ireland',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { name: 'description', content: "Affordable Web Design company in Belfast, Northern Ireland. We work with Start-Ups & Established Businesses, specialising in Web Design, SEO & Digital Marketing" },
      { name: 'og-title', content: "Element Seven - Affordable Web Design Belfast, Northern Ireland" },
      { name: 'og-image', content: "https://elementseven.co/img/og1.jpg" },
      { name: 'og:description', content: "Affordable Web Design company in Belfast, Northern Ireland. We work with Start-Ups & Established Businesses, specialising in Web Design, SEO & Digital Marketing" },
      { name: 'og-url', content: "https://elementseven.co" },
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Caveat:wght@700&family=Noto+Sans&family=Teko:wght@500&display=swap'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/img/favicon/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/scss/app.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/client-only.js', mode: 'client' },
    { src: '~/plugins/server-only.js', mode: 'server' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  babel: {
    presets(env, [ preset, options ]) {
      return [
        ["@babel/preset-env", {"loose": true, "shippedProposals": true}]
      ]
    },
    plugins(env, [ preset, options ]) {
      return [
        ["@babel/plugin-syntax-dynamic-import"],
        ["@babel/plugin-proposal-private-methods", { "loose": true }]
      ]
    },
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    '@nuxtjs/recaptcha',
  ],

  recaptcha: {
    hideBadge: true,
    siteKey:"6LcM2tMZAAAAAOg9KcJeKqSNz2sOe2rfbm2vZ6TD",
    version: 3,
    size: "invisible"
  },
  styleResources: {
    scss: [
      './assets/scss/variables.scss',
      './assets/scss/mixins.scss'
    ]
  },

  router: {
    base: '/',
    trailingSlash: false,
    scrollBehavior: function(){
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    },
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'Work',
        path: '/our-work',
        component: resolve(__dirname, 'pages/work/index.vue')
      }),
      routes.push({
        name: 'WorkPost',
        path: '/our-work/:workSlug',
        component: resolve(__dirname, 'pages/work/post.vue')
      }),
      routes.push({
        name: 'Blog',
        path: '/blog-posts',
        component: resolve(__dirname, 'pages/blog/index.vue')
      }),
      routes.push({
        name: 'BlogPost',
        path: '/blog-posts/:blogDate/:blogSlug',
        component: resolve(__dirname, 'pages/blog/post.vue')
      }),
      routes.push({
        name: 'Contact',
        path: '/contact',
        component: resolve(__dirname, 'pages/contact.vue')
      }),
      routes.push({
        name: 'Services',
        path: '/services',
        component: resolve(__dirname, 'pages/services/index.vue')
      }),
      routes.push({
        name: 'LandingPage',
        path: '/what-we-do/:lpSlug',
        component: resolve(__dirname, 'pages/landingPage.vue')
      }),
      routes.push({
        name: 'Tandcs',
        path: '/terms-and-conditions',
        component: resolve(__dirname, 'pages/tandcs.vue')
      }),
      routes.push({
        name: 'TandcsOld',
        path: '/terms-and-conditions-28-10-2019',
        component: resolve(__dirname, 'pages/tandcs-28-10-2019.vue')
      })
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
