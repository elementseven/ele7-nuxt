// import "babel-polyfill";
import 'waypoints/lib/noframework.waypoints.min.js';
import'./modernizr-custom.js';
import './cookieConsent.js';
import Vue from 'vue';
import VueLazyload from "vue-lazyload";
import InfiniteLoading from 'vue-infinite-loading';
import 'bootstrap/dist/js/bootstrap.min.js';
window.$ = window.jQuery = require('jquery');
window.AOS = require('aos');
window.axios = require("axios");
window.Vue = Vue;
AOS.init();

Vue.use(VueLazyload, {
  lazyComponent: true
});

Vue.use(InfiniteLoading);


window.initMap = function () {
	var marker;
	var map;
  var mapstyle = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#09ffc3"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#ff09d8"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]}];
  var mapDiv = document.getElementById('map');
  var ellatlng = {lat: 54.583218764469976, lng: -5.944762725814788};
  var myIcon = {
    url: "/img/icons/map_pin2.png",
    size: new window.google.maps.Size(100, 165),
    origin: new window.google.maps.Point(0, 0),
    anchor: new window.google.maps.Point(50, 110),
    scaledSize: new window.google.maps.Size(100, 165)
  };
  // var myIcon = new google.maps.MarkerImage("/img/icons/map_pin.png", null, null, null, new google.maps.Size(50, 93));
  map = new window.google.maps.Map(mapDiv, {
    center: ellatlng,
    scrollwheel: false,
    mapTypeControl: false,
    draggable: false,
    streetViewControl: false,
    fullscreenControl: false,
    zoom: 17
  });
  // Add a marker to the map showing where the user is located
  marker = new window.google.maps.Marker({
      position: ellatlng,
      map: map,
      animation: window.google.maps.Animation.DROP,
      flat: true,
      icon: myIcon,
      draggable: false,
      title: "Element Seven",
      'optimized': false
  });
  map.set('styles', mapstyle);
}